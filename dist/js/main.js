let generatedData;
let dictionaryData;
let pupilsData;
let NormPupilsData = [];
//-------------------------------
let CorpsesMap
let PlacesMap
let AudiencesMap
//-------------------------------

let temp_place_id;
let temp_corps_id;

const SERVER_API_URL = "https://lyceumexams.herokuapp.com/api/";
const SERVER_API_URL_DICTIONARY = SERVER_API_URL + "dictionary";
const SERVER_API_URL_GENERATE = SERVER_API_URL + "generate";
const SERVER_API_URL_PUPILS = SERVER_API_URL + "pupils";

const btn_generate_id = "#btn-generate";
const corps_select_id = "#corps-select";
const profile_select_id = "#profile-select";
const audience_select_id = "#audience-select";

 const btn_generate = $(btn_generate_id);
 const corps_select = $(corps_select_id);
 const profile_select = $(profile_select_id);
 const audience_select = $(audience_select_id);

btn_generate.click(function () 
{ 
    var time = performance.now();
    //----------------------------------
    LoadAllData();
    //----------------------------------
    time = performance.now() - time;
    console.log('loadDataTime =', time);
});

corps_select.change(function () 
{
    temp_corps_id = this.value;
    LoadCorpsSelect();
});

profile_select.change(function () 
{
    temp_place_id = this.value;    
    temp_place_id != '?' ? LoadPlaceSelect() : LoadCorpsSelect();
});

audience_select.change(function () 
{
    let audience_id = this.value;
    audience_id !="?" ? PupilsTableLoader.LoadByAudienceId(audience_id) : LoadPlaceSelect();
});

function LoadCorpsSelect(){
    SelectsLoader.LoadProfileData(temp_corps_id);
    SelectsLoader.LoadAudienceDataAll(temp_corps_id);
    PupilsTableLoader.LoadByCorpsAlias(temp_corps_id);    
    CorpsAudiencesTableLoader.LoadByCorpsAlias(temp_corps_id);
}

function LoadPlaceSelect(){
    CorpsAudiencesTableLoader.LoadByPlaceId(temp_place_id);
    PupilsTableLoader.LoadByPlaceId(temp_place_id);
    SelectsLoader.LoadAudienceData(temp_place_id);
}

function LoadPupilsData() 
{
    $.getJSON(SERVER_API_URL_PUPILS, function (data) 
    {
        pupilsData = data;
        getPupilsNormData();
    });
}

function LoadDictionaryData()
{
    $.getJSON(SERVER_API_URL_DICTIONARY, function (data) 
    {
        dictionaryData = data;
    });
}

function LoadGeneratedData() 
{
    $.getJSON(SERVER_API_URL_GENERATE, function (data) 
    {
        generatedData = data;
        SelectsLoader.LoadCorpsData();
    });
}

function LoadAllData(){
    LoadGeneratedData();
    LoadPupilsData();
    LoadDictionaryData();
}

function getAudienceById(audience_id) 
{
    return dictionaryData.audiences[audience_id];
}

function getProfileById(profile_id)
{
    return dictionaryData.profiles[profile_id];
}

function getPupilsNormData() 
{
    pupilsData.forEach(pupil=>{
        NormPupilsData.push
        ({
            fullName:
                pupil.firstName +
                " " +
                pupil.lastName +
                " " +
                pupil.parentName, 

            audience_id: pupil.audience,
            corps_alias: pupil.corps,
            bel_img:     pupil.needBel,
            place_id:    pupil.place,

            audience_name: getAudienceById(pupil.audience),
            profile_name: getProfileById(pupil.profile)  
        });
    });
}

let SelectsLoader = (function(){
    const select_default_header = '<option value="?" selected="selected">Все</option>';
    let select_data;

    return {
        LoadCorpsData: LoadCorpsData, 
        LoadProfileData: LoadProfileData,
        LoadAudienceDataAll: LoadAudienceDataAll,
        LoadAudienceData: LoadAudienceData,
    }       
        function LoadCorpsData() 
        {
            clearSelect(corps_select);
            generatedData.corpses.forEach(corps => {
                select_data += getStringSelectData(corps.alias, corps.name);
            });
            corps_select.append(select_data);
        }

        function LoadProfileData(corps_alias)
        {
            clearSelect(profile_select);
            generatedData.corpses.forEach(corps => {
                if (corps_alias == corps.alias)
                {
                    corps.places.forEach(place => {
                        select_data += getStringSelectData(place._id, place.code);
                    });
                    profile_select.append(select_data);
                }
            });
        }

        function LoadAudienceDataAll(corps_alias)
        {
            clearSelect(audience_select);
            generatedData.corpses.forEach(corps=>{
                if (corps_alias == corps.alias){
                    corps.places.forEach(place=>{
                       this.LoadAudienceData(place._id);
                    });
                }
            });
        }

        function LoadAudienceData(place_id)
        {
            clearSelect(audience_select);
            generatedData.corpses.forEach(corps=>{
                corps.places.forEach(place=>{
                    if (place_id == place._id){
                        place.audience.forEach(audience=>{
                            select_data += getStringSelectData(audience._id, audience.name);
                        });
                        audience_select.append(select_data);
                    }
                });
            });
        }

        function clearSelect(JSelectObject){
            select_data = "";
            JSelectObject.empty();
            JSelectObject.append(select_default_header);
        }
    
        function getStringSelectData (value, name){
           return "<option value=" +
                value +
            ">" +
                name +
            "</option>"
        }
})();


let PupilsTableLoader = (function(){
    const table_persons_id = "#table-persons";
    const table_persons = $(table_persons_id);
    const persons_table_header = "<tr><th>#</th><th>ФИО</th><th>Каб.</th><th>Профиль</th><th>Бел</th></tr>";
    let counter = 1;
    let pupils_table_data;

    return {
        LoadByAudienceId : LoadByAudienceId,
        LoadByPlaceId : LoadByPlaceId, 
        LoadByCorpsAlias : LoadByCorpsAlias 
    }

    function LoadByAudienceId(audience_id){
        NormPupilsData.forEach(pupil=>{
            if (audience_id == pupil.audience_id){
                pupils_table_data += addDataToTablePersons(counter, pupil);
                counter++;
            }
        });
        clearTable();
    }

    function LoadByPlaceId(place_id){
        NormPupilsData.forEach(pupil=>{
            if (place_id == pupil.place_id){
                pupils_table_data += addDataToTablePersons(counter, pupil);
                counter++;
            }
        });
        clearTable();
    }

    function LoadByCorpsAlias(corps_alias){
        NormPupilsData.forEach(pupil=>{
            if (corps_alias == pupil.corps_alias){
                pupils_table_data += addDataToTablePersons(counter, pupil);
                counter++;
            }
        });
        clearTable();
    }

    function clearTable() 
    {
        table_persons.empty();
        table_persons.append(persons_table_header);
        table_persons.append(pupils_table_data);
        counter = 1;
        pupils_table_data = "";
    }

    function addDataToTablePersons(counter, pupil)
    {
        return  "<tr>" +
                    "<td>" +
                        counter +
                    "</td>" +
                    "<td>" +
                        pupil.fullName +
                    "</td>" +
                    "<td>" +
                        pupil.audience_name +
                    "</td>" +
                    "<td>" +
                        pupil.profile_name +
                    "</td>" +
                    "<td>" +
                        pupil.bel_img +
                    "</td>" +
                "</tr>"
    }
})();


let CorpsAudiencesTableLoader = (function(){

    const corps_audiences_table_id =  "#corps-audiences-table";
    const corps_audiences_table = $(corps_audiences_table_id);
    const corps_audience_table_header = "<tr><th>#</th><th>Количество</th><th>Max</th><th>Бел</th></tr>";

    let audience_table_data;

    return {
        LoadByCorpsAlias : LoadByCorpsAlias,
        LoadByPlaceId : LoadByPlaceId
    }
    
    function LoadByPlaceId(place_id)
    {
        generatedData.corpses.forEach(corps=>{
            corps.places.forEach(place=>{
                if (place_id == place._id)
                {
                    place.audience.forEach(audience=>{
                        audience_table_data += addDataToCorpsAudiencesTable(audience)
                    });   
                }
            });
        });
        clearTable();
    }

    function LoadByCorpsAlias(corps_alias)
    {
        generatedData.corpses.forEach(corps=>{
            if (corps_alias == corps.alias){
                corps.places.forEach(place=>{
                    place.audience.forEach(audience=>{
                        audience_table_data += addDataToCorpsAudiencesTable(audience)
                    });
                });
            }
        });
        clearTable();
    }


    function clearTable() 
    {
        corps_audiences_table.empty();
        corps_audiences_table.append(corps_audience_table_header);
        corps_audiences_table.append(audience_table_data);
        audience_table_data = "";
    }

    function addDataToCorpsAudiencesTable(audience)
    {
        return  "<tr>" +
                    "<td>" +
                        audience.name +
                    "</td>" +
                    "<td>" +
                        audience.count +
                    "</td>" +
                    "<td>" +
                        audience.max +
                    "</td>" +
                    "<td>" +
                        audience.bel +
                    "</td>" +
                "</tr>"   
    }
})();